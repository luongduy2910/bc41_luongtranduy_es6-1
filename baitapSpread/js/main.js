// TODO: sử dụng arrow function để tạo hàm hiệu ứng jump text 
// * Sử dụng spread operator để tách kí tự => trả về mảng => render mảng thành các span 

let jumpTextHover = () => {
    let headingEl = document.querySelector('.heading').innerText ; 
    let listchar = [...headingEl]; 
    var contentHTML = "" ; 
    listchar.forEach(item => {
        let contentSpan = `<span>${item}</span>`
        contentHTML += contentSpan ; 
    })
    document.querySelector('.heading').innerHTML = contentHTML ; 
}

jumpTextHover() ; 
