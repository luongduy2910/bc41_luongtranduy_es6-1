// TODO: Viết hàm tính điểm trung bình sử dụng arrow function và rest parameter 
// * Khi muốn truyền tham số mà không xác định số lượng của tham số -> sử dụng rest parameter
// * Tham số truyền vào sẽ thành mảng 


let getElNum = (id) => {
    return document.getElementById(id).value * 1 ; 
}
let tinhDTB1 = () => {
    let diemToan = getElNum('inpToan') ;
    let diemLy = getElNum('inpLy') ;
    let diemHoa = getElNum('inpHoa') ;
    let ketQua = tinhDTB(diemToan , diemLy , diemHoa) ; 
    document.getElementById('tbKhoi1').innerHTML = ketQua ;
    
}



let tinhDTB2 = () => {
    let diemVan = getElNum('inpVan') ;
    let diemSu = getElNum('inpSu') ;
    let diemDia = getElNum('inpDia') ;
    let diemTiengAnh = getElNum('inpEnglish') ;
    let ketQua = tinhDTB(diemVan , diemSu , diemDia , diemTiengAnh) ; 
    document.getElementById('tbKhoi2').innerHTML = ketQua ;
    
}


